//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create a schema
const userSchema = new Schema({
	firstName:{
		type: String,
		trim: true
	},
	lastName:{
		type: String,
		trim: true
	},
	email:{
		type: String,
		required: true,
		unique: true,
		minlength: 5
	},
	password:{
		type: String,
		required: true
	},
	isAdmin:{
		type: Boolean,
		default: false
	}
});

//export schema as model
module.exports = mongoose.model("User", userSchema);
