//MODELS
const User = require('../models/user');
const Service = require('../models/service');
const Schedule = require('../models/schedule');
const Booking = require('../models/booking');

//dependency
const bcrypt = require("bcrypt");

const resolvers = {
	User: {
		bookings: ({ _id }, args) => {
			return Booking.find({ userId: _id });
		}
	},
	Service: {
		bookings: ({ _id }, args) => {
			return Booking.find({ serviceId: _id });
		}
	},
	Schedule: {
		bookings: ({ _id }, args) => {
			return Booking.find({ scheduleId: _id });
		}
	},
	Booking: {
		user: ({ userId }, args) => {
			return User.findById(userId);
		},
		service: ({ serviceId }, args) => {
			return Service.findById(serviceId);
		},
		schedule: ({ scheduleId }, args ) => {
			return Schedule.findById(scheduleId);
		}
	},
	Query: {
		users: () => {
			return User.find({});
		},
		services: () => {
			return Service.find({});
		},
		schedules: () => {
			return Schedule.find({});
		},
		bookings: () => {
			return Booking.find({});
		},
		user: (parent, { id }) => {
			return User.findById(id);
		},
		booking: (parent, { id }) => {
			return Booking.findById(id);
		},
		service: (parent, { id }) => {
			return Service.findById(id);
		},
		schedule: (parent, { id }) => {
			return Schedule.findById(id);
		}
	},

	Mutation: {
		registerUser: (parent, {firstName, lastName, email, password}) => {
			//hash
			let user = new User({
				firstName,
				lastName,
				email,
				password: bcrypt.hashSync(password, 8)
			});

			return user.save().then((user, err) => {
				return err ? false : true;
			});
		},
		// loginUser: (parent, { email, password }) => {
		// 	//validation
		// 	let query = User.findOne({ email });
		// 	return query.then(user => {
		// 		if (user === null){
		// 			return null;
		// 		}

		// 		//unhash password
		// 		let isPasswordMatched = bcrypt.compareSync(password, user.password);

		// 		//create login token
		// 		if (isPasswordMatched) {
		// 			user.token = auth.createToken(user.toObject());
		// 			return user;
		// 		} else {
		// 			return null;
		// 		}
		// 	});
		// },
		storeService: (parent, { title, url, comments }) => {
			//validation
			//verify and decode token
			let service = new Service({
				title,
				url,
				comments
			});
			return service.save();
		},
		storeSchedule: (parent, { month, date, isAvailable, bookings}) => {
			//validation
			//verify and decode token
			let schedule = new Schedule({
				month,
				date,
				isAvailable,
				bookings
			});
			return schedule.save();
		},
		storeBooking: (parent, { isApproved, isActive, userId, serviceId, scheduleId}) => {
			//verify and decode token
			//check if userId, serviceId, scheduleId exists
			let booking = new Booking({
				isApproved,
				isActive,
				userId,
				serviceId,
				scheduleId
			});
			return booking.save();
		},

		updateUser:(parent, {id, firstName, lastName, email, password}) => {
			return User.findByIdAndUpdate(id, {firstName, lastName, email, password})
		},

		updateService:(parent, {id, title, url, comments}) => {
			return Service.findByIdAndUpdate(id, {title, url, comments})
		},

		updateSchedule: (parent, { id, month, date, isAvailable}) => {
			return Schedule.findByIdAndUpdate(id, {month, date, isAvailable})
		},

		updateBooking: (parent, { id, isApproved, isActive, userId, serviceId, scheduleId}) => {
			return Booking.findByIdAndUpdate(id, {isApproved, isActive, userId, serviceId, scheduleId})
		},

		destroyUser: (parent, {id}) => {
			return User.findByIdAndRemove(id)
		},

		destroyService: (parent, {id}) => {
			return Service.findByIdAndRemove(id)
		},

		destroySchedule: (parent, {id}) => {
			return Schedule.findByIdAndRemove(id)
		},

		destroyBooking: (parent, {id}) => {
			return Booking.findByIdAndRemove(id)
		}
	}
};

module.exports = resolvers;
