const { gql } =  require("apollo-server-express");


const typeDefs = gql`
	type User {
		id: ID
		firstName: String
		lastName: String
		email: String
		password: String
		isAdmin: Boolean
		bookings: [Booking]
	}

	type Service {
		id: ID
		title: String
		url: String
		comments: String
		bookings: [Booking]
	}

	type Schedule {
		id: ID
		month: String
		date: String
		isAvailable: Boolean
		bookings: [Booking]
	}

	type Booking {
		id: ID
		isApproved: Boolean
		isActive: Boolean
		userId: String
		user: User
		serviceId: String
		service: Service
		scheduleId: String
		schedule: Schedule
	}

	type Query {
		users: [User!]
		services: [Service!]
		schedules: [Schedule!]
		bookings: [Booking!]
		user(id: ID!): User
		schedule(id: ID!): Schedule
		service(id: ID!): Service
		booking(id: ID!): Booking
	}

	type Mutation {
		registerUser(
			firstName: String
			lastName: String
			email: String
			password: String
		): Boolean

		storeService(
			title: String
			url: String
			comments: String
		): Service

		storeSchedule(
			month: String
			date: String
			isAvailable: Boolean
		): Schedule

		storeBooking(
			isApproved: Boolean
			isActive: Boolean
			userId: String
			serviceId: String
			scheduleId: String
		): Booking

		updateUser(
			id: ID!
			firstName: String
			lastName: String
			email: String
			password: String
		): User

		updateService(
			id: ID!
			title: String
			url: String
			comments: String
		): Service

		updateSchedule(
			id: ID!
			month: String
			date: String
			isAvailable: Boolean
		): Schedule

		updateBooking(
			id: ID!
			isApproved: Boolean
			isActive: Boolean
			userId: String
			serviceId: String
			scheduleId: String
		): Booking

		destroyUser(id: ID!): User

		destroyService(id: ID!): Service

		destroySchedule(id: ID!): Schedule

		destroyBooking(id: ID!): Booking
	}
`;

module.exports = typeDefs;

