//1) declare dependencies
const express = require("express");
const app = express();
const cors = require('cors');
const { ApolloServer } = require("apollo-server-express");
const typeDefs = require("./typeDefs");
const resolvers = require("./resolvers");

//3) connect to db
const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/booking', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true
});
mongoose.connection.once('open',()=>{
	console.log("Now connected to local MongoDB server.");
});

//apollo server
const server = new ApolloServer({
	typeDefs,
	resolvers,
	playground: true,
	introspection: true
})

server.applyMiddleware({
	app,
	path: "/graphql"
})

//connect the frontend and backend
app.use(cors());

//2) initialize server
const port = 4001;
// console.log(`${port}`);
app.listen(port, ()=>{
	console.log(`Now listening for requests on port ${port}`);
});

//http://localhost:4001/graphql
//http://localhost:4000/user/id